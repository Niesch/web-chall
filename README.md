# web-chall
---

The goal is to execute JavaScript without leaving the site.

In order to "pass", the JavaScript must be executed in a browser supporting `Content-Security-Policy` (CSP). Latest Firefox, Chrome and Internet Explorer all support this feature.

- To get this challenge, run `go get gitlab.com/Niesch/web-chall` with your `$GOPATH` set properly. Having a proper `$GOPATH` is crucial as it bundles a library it uses itself.
- To build this challenge, run `go build` in the root directory of the project.
- To run this challenge, run `./web-chall`. It will bind to port 8080 so root access isn't required (nor recommended).

