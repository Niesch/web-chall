package main

import (
	"fmt"
	"gitlab.com/Niesch/web-chall/http"
	"log"
	"text/template"
)

type Content struct {
	Author string
	Body   string
}

func main() {
	http.HandleFunc("/", indexHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	c := Content{}
	if r.Method == "POST" {
		r.ParseForm()
		if len(r.Form["uh"]) == 0 {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, "ge mig ett namn mannen")
			return
		}
		if len(r.Form["ah"]) == 0 {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, "ge mig lite innehåll mannen")
			return
		}
		c.Author = r.Form["uh"][0]
		c.Body = r.Form["ah"][0]
		fmt.Printf("%s sent author: %s.\n", r.RemoteAddr, c.Author)
	} else {
		c.Author = ""
		c.Body = ""
	}

	w.Header().Set("Content-Security-Policy", "default-src 'self'; script-src 'none'")
	w.Header().Set("🐺-Powered-By", "😼")
	w.Header().Set("Author", c.Author)

	t := template.Must(template.New("template.html").ParseFiles("template.html"))

	err := t.Execute(w, c)
	if err != nil {
		fmt.Fprintf(w, "%s", err)
	}
}
